﻿namespace Practice1.Data.Repositories
{
    using System.Collections.Generic;
    using System.Linq;

    public class UsersRepository
    {
        public List<Users> GetUsers()
        {
            using(var ctx = new Practice1Entities())
            {
                return ctx.Users.DefaultIfEmpty().ToList();
            }
        }
    }
}

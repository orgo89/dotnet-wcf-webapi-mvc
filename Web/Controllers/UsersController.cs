﻿namespace Practice1.Clients.Web.Controllers
{
    using Practice1.Commons.Dtos;
    using RestSharp;
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class UsersController : Controller
    {
        public UsersController()
        {
            ViewBag.Users = new List<DtoUser>();
        }

        // GET: Users
        public ActionResult Index()
        {
            try
            {
                var request = new RestRequest("users", Method.GET);
                var response = this.Request<DtoGetUsersResponse>(request);

                if (response != null && response.Status == ProcessStatus.SUCCESSFUL && response.Users.Count > 0)
                {
                    ViewBag.Users = response.Users;
                }
            }
            catch (Exception ex)
            {
                ViewBag.Erro = ex.Message;
            }
            

            return View();
        }

        private T Request<T>(RestRequest request) where T : new()
        {
            var client = new RestClient();
            client.BaseUrl = new System.Uri("http://localhost:50261/api");
            //client.Authenticator = new HttpBasicAuthenticator(_accountSid, _secretKey);
            //request.AddParameter("AccountSid", _accountSid, ParameterType.UrlSegment); // used on every request
            var response = client.Execute<T>(request);

            if (response.ErrorException != null)
            {
                const string message = "Error retrieving response.  Check inner details for more info.";
                var twilioException = new ApplicationException(message, response.ErrorException);
                throw twilioException;
            }
            return response.Data;
        }
    }
}
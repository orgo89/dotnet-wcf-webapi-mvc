﻿namespace Practice1.Services.LocalService
{
    using Practice1.Commons.Dtos;
    using System.Collections.Generic;
    using System.ServiceModel;

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUsersLocalService" in both code and config file together.
    [ServiceContract]
    public interface IUsersLocalService
    {
        [OperationContract]
        DtoGetUsersResponse GetUsers();
    }
}

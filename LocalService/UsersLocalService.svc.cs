﻿namespace Practice1.Services.LocalService
{
    using Practice1.Business.Management;
    using Practice1.Commons.Dtos;
    using System.Collections.Generic;
    
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "UsersLocalService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select UsersLocalService.svc or UsersLocalService.svc.cs at the Solution Explorer and start debugging.
    public class UsersLocalService : IUsersLocalService
    {
        public DtoGetUsersResponse GetUsers()
        {
            try
            {
                return new DtoGetUsersResponse
                {
                    Users = new UsersManagement().GetUsers(),
                    Status = ProcessStatus.SUCCESSFUL
                };
            }
            catch (System.Exception ex)
            {
                return new DtoGetUsersResponse
                {
                    Status = ProcessStatus.INTERNALERROR,
                    Message = ex.Message
                };
            }
        }
    }
}

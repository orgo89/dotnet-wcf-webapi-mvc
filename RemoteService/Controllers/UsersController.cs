﻿namespace Practice1.Service.RemoteService.Controllers
{
    using Practice1.Commons.Dtos;
    using System.Threading.Tasks;
    using System.Web.Http;

    public class UsersController : ApiController
    {
        public async Task<IHttpActionResult> GetAll()
        {
            return await Task.Factory.StartNew(() => {
                try
                {
                    return this.Ok(new UsersLocalService.UsersLocalServiceClient().GetUsers());
                }
                catch (System.Exception ex)
                {
                    return this.Ok(new DtoGetUsersResponse
                    {
                        Status = ProcessStatus.INTERNALERROR,
                        Message = ex.Message
                    });
                }
            });            
        }
    }
}

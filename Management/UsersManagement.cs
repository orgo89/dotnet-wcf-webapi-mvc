﻿namespace Practice1.Business.Management
{
    using Practice1.Commons.Dtos;
    using Practice1.Data.Repositories;
    using System.Collections.Generic;
    using System.Linq;

    public class UsersManagement
    {
        public List<DtoUser> GetUsers()
        {
            var users = new UsersRepository().GetUsers();

            if (users != null && users.All(u => u != null))
            {
                return users.Select((usr) =>
                {
                    return new DtoUser
                    {
                        Id = usr.Id,
                        Age = usr.Age,
                        Apm = usr.Apm,
                        App = usr.App,
                        Name = usr.Name
                    };
                }).ToList();
            }

            return null;
        }

    }
}

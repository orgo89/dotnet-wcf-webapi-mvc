﻿namespace Practice1.Commons.Dtos
{
    public class DtoUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string App { get; set; }
        public string Apm { get; set; }
        public byte? Age { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice1.Commons.Dtos
{
    public class DtoResponse
    {
        public string Message { get; set; }

        public ProcessStatus Status { get; set; }
    }
}

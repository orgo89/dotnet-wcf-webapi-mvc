﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice1.Commons.Dtos
{
    public class DtoGetUsersResponse : DtoResponse
    {
        public List<DtoUser> Users { get; set; }
    }
}
